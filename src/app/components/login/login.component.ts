import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  passwordControl: FormControl
  authError: string | null = null;


  constructor(private authService: AuthService, private router: Router) 
  {
    this.passwordControl = new FormControl(null, [Validators.required, Validators.minLength(3)]);


    this.loginForm = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: this.passwordControl

    });

  }
  private passwordMatch(form: AbstractControl): ValidationErrors | null {
    if (form.value?.password != form.value?.passwordConfirmation) {
      return { passwordConfirmationMustMatch: true };
    } else {
      return null
    }
  }

  ngOnInit(): void {
  }
  logIn(){
    this.authService.logIn(this.loginForm.value).subscribe(success =>{
      
      if (success) {
        this.router.navigate(['/recipes']);
      } else {
        this.authError = "Invalid credentials!";
      }
    })

  }

}
