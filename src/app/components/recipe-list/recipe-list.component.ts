import { Component, OnInit } from '@angular/core';
import { Recipe } from 'src/app/models/recipe.model';
import { RecipeService } from '../../services/recipe.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {

  recipes: Recipe[]= [];
  getRecipes() 
  {
    this.recipeService.getRecipes().subscribe(success =>
    {
      if(success)
      {
        this.recipes= success as Recipe[]
      } else{
        console.log("couldnt load recipe")
      }
    });
  }

  constructor(private recipeService: RecipeService, private router: Router) { 
    
  }

  ngOnInit(): void {
    this.getRecipes()
  }

  recipedeleted(recipe: Recipe) {
    this.recipeService.deleteRecipe(recipe).subscribe(success =>
      {
        if(success)
        {
          this.getRecipes()
        } else{
          console.log("couldnt delete recipe")
        }
      });
  }
  recipeEdited(recipe: Recipe) {
    this.router.navigate(['recipe',recipe.id])
  }


}
