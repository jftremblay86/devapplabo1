import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Recipe } from 'src/app/models/recipe.model';
import { RecipeService } from 'src/app/services/recipe.service';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-recipe-form',
  templateUrl: './recipe-form.component.html',
  styleUrls: ['./recipe-form.component.css']
})
export class RecipeFormComponent implements OnInit {

  recipeForm: FormGroup;
  authError: string | null = null;
  id!: string | null;

  constructor(private recipeService: RecipeService, private router: Router,private route: ActivatedRoute, private authService:AuthService) {
    this.recipeForm = new FormGroup({
      name: new FormControl(null, [Validators.required, Validators.minLength(3)]),
      category: new FormControl(),
      description: new FormControl(null, [Validators.required, Validators.minLength(5)]),
    });
  }

  ngOnInit(): void 
  {
    this.id = this.route.snapshot.paramMap.get('id');
    if(this.id!=null)
    {
      console.log("todi")
      let currentRecipe:Recipe
      this.recipeService.getRecipe(this.id).subscribe(success =>
        {
        if (success)
          {
          currentRecipe=success as Recipe
          
          this.recipeForm.controls['name'].setValue(currentRecipe.name)
          this.recipeForm.controls['category'].setValue(currentRecipe.category)
          this.recipeForm.controls['description'].setValue(currentRecipe.description)
          }else{
          console.log('invalid')
          }
      })
    }
  }


  



  createRecipe() {

    if(this.id== null){
      let recipe = new Recipe(this.recipeForm.value)
      // recipe.category=this.recipeForm.get.('category')?.value;
       this.recipeService.createRecipe(recipe).subscribe(success =>{
       
         if (success) {
           this.router.navigate(['/recipes']);
         } else {
           this.authError = "Invalid credentials!";
         }
       })
         this.recipeForm.reset();   
     }else{
      let recipe = new Recipe(this.recipeForm.value) 
      recipe.id = this.id
      recipe.email = this.authService.currentUser!.email
      this.recipeService.updateRecipe(recipe).subscribe(success =>{
       
        if (success) {
          this.router.navigate(['/recipes']);
        } else {
          this.authError = "Invalid credentials!";
        }
      })
        this.recipeForm.reset(); 



     }

    }


}
