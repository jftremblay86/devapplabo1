import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Recipe } from 'src/app/models/recipe.model';
import { Router } from '@angular/router';

@Component({
  selector: '[app-recipe-list-item]',
  templateUrl: './recipe-list-item.component.html',
  styleUrls: ['./recipe-list-item.component.css']
})
export class RecipeListItemComponent implements OnInit {

  @Input() recipe!: Recipe;
  @Output() recipedeleted = new EventEmitter();
  @Output() recipeEdited = new EventEmitter();


  constructor(private router: Router) { }

  ngOnInit(): void {
    
  }

  recipeCategory(number:number)
  {
    if(this.recipe.category == 0){
      return "BreakFast" 
    }
    if(this.recipe.category ==1){
      return "Lunch" 
    }
    if(this.recipe.category ==2){
      return "Diner" 
    }
    return null
  }

  deleted() {

    this.recipedeleted.emit(this.recipe);
  }
  edited(){
    
    this.recipeEdited.emit(this.recipe);
  
  }


}
