import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/app/services/auth.service';
import { RecipeService } from '../../services/recipe.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  language!:string|null

  private readonly LANGUAGE_KEY = 'recipeEasy.language';

  constructor(public authService: AuthService, private router: Router,private recipeService:RecipeService,private translate: TranslateService ) { 

    translate.addLangs(['en', 'fr'])
    translate.setDefaultLang('en');
    this.language=localStorage.getItem(this.LANGUAGE_KEY)??'en';
    translate.use(this.language);

  }

  changelanguage(languageEvent: EventTarget|null){
    if(languageEvent instanceof HTMLSelectElement){
      let langue = (languageEvent as HTMLSelectElement).value;
      this.translate.use(langue)
      localStorage.setItem(this.LANGUAGE_KEY, langue)
    }
  }

  logOut() {
    this.authService.logOut();

    this.router.navigate(['/']);
  }


}
