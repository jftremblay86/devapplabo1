import { Routes, CanActivateChild } from '@angular/router';
import { LoginComponent } from '../login/login.component';
import { PageNotFoundComponent } from '../page-not-found/page-not-found.component';
import { ProfileComponent } from '../profile/profile.component';
import { RecipesComponent } from '../recipes/recipes.component';
import { SignupComponent } from '../signup/signup.component';
import { ProfileDetailsComponent } from '../profile-details/profile-details.component';
import { AuthGuard } from '../../guards/auth.guard';
import { RecipeFormComponent } from '../recipe-form/recipe-form.component';
import { UnloginGuard } from '../../guards/unlogin.guard';



export const routes: Routes = 
[
  { path: '', canActivateChild: [UnloginGuard], children:[
    {path: '', component: LoginComponent},
    { path: 'signup', component: SignupComponent },
  ]},
    { path: '', canActivateChild: [AuthGuard], children:[
      { path: 'recipes', component: RecipesComponent },
      { path: 'profile', component: ProfileComponent },
      { path: 'profile/:email', component: ProfileDetailsComponent }, 
      {path: 'recipe/new', component: RecipeFormComponent},
      {path: 'recipe/:id', component: RecipeFormComponent}
    ]},

    { path: '**', component: PageNotFoundComponent }
  ];