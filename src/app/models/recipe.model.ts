import {custom, JSONObject, optional, required} from 'ts-json-object'

export class Recipe extends JSONObject {
    @required
    name!: string;
  
    @required
    @custom((recipe: Recipe, key: string, value: number) => {
      return +value;
    })
    category!: number;

    @required
    description!: string;

    @optional
    id!: string;

    @optional
    email!: string;



  }