import { stringify } from '@angular/compiler/src/util';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Recipe } from '../models/recipe.model';
import { AuthService } from './auth.service';
import { MarthaRequestService } from './martha-request.service';
import { map, tap } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})

export class RecipeService 
{

  private _currentRecipe : Recipe | null = null;

  get currentRecipe(): Recipe | null {
    return this._currentRecipe;
  }


constructor(private authService: AuthService, private martha: MarthaRequestService) 
{
}


getRecipes(): Observable<Recipe[]| null>{
  return this.martha.select('recipes-list', this.authService.currentUser).pipe(
    map(data =>{
      console.log('recipeService',data);
      if(data){
        return data;
      }else{
        return null;
      }
    })
  );
}

getRecipe(id: string): Observable<Recipe>{
  return this.martha.select('recipes-read',{"id":id,"email":this.authService.currentUser?.email}).pipe(
    map(data =>{
      console.log('recipeService',data);
      if(data){
        return data[0];
      }else{
        return null;
      }
    })
  );
}



createRecipe(recipe: Recipe): Observable<boolean>
{
  recipe.email =this.authService.currentUser!.email
  recipe.id =Math.random().toString(16).substring(2)
  return this.martha.insert('recipes-create', recipe).pipe(
    map(result => 
    {
      if(result?.success)
      {
        return true;
      }else{
        return false;
      }
    })
  )
}

deleteRecipe(recipe: Recipe):Observable<boolean>
{
  recipe.email =this.authService.currentUser!.email
  return this.martha.insert('recipes-delete', recipe).pipe(
    map(result => 
    {
      if(result?.success)
      {
        return true;
      }else{
        return false;
      }
    })
  )
}

updateRecipe(recipe: Recipe):Observable<boolean>
{
  recipe.email =this.authService.currentUser!.email
  return this.martha.insert('recipes-update', recipe).pipe(
    map(result => 
    {
      if(result?.success)
      {
        return true;
      }else{
        return false;
      }
    })
  )
}


}
